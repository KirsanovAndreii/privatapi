package com.example.ankir.privatbankapi;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by ankir on 30.05.2017.
 */

public class Retrofit {

    private static final String ENDPOINT = "https://api.privatbank.ua";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/p24api/pboffice?json")
        void getApiPrivat(Callback<List<ResponsePrivatApi>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getApiPrivat(Callback<List<ResponsePrivatApi>> callback) {
        apiInterface.getApiPrivat(callback);
    }
}
