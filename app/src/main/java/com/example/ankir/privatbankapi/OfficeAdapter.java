package com.example.ankir.privatbankapi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


/**
 * Created by ankir on 30.05.2017.
 */

public class OfficeAdapter extends RecyclerView.Adapter<OfficeAdapter.ViewHolder> {

    private List<ResponsePrivatApi> officesPrivats;
    private Context context;

    public OfficeAdapter(Context context, List<ResponsePrivatApi> officesPrivats) {
        this.officesPrivats = officesPrivats;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_office, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {
        ResponsePrivatApi office = officesPrivats.get(pos);
        viewHolder.name.setText(office.name);
        viewHolder.city.setText(office.city);
        viewHolder.address.setText(office.address);
        viewHolder.phone.setText(office.phone);
    }

    @Override
    public int getItemCount() {
        return officesPrivats.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView city;
        private TextView address;
        private TextView phone;

        public ViewHolder(View item) {
            super(item);
            name = (TextView) item.findViewById(R.id.name);
            city = (TextView) item.findViewById(R.id.city);
            address = (TextView) item.findViewById(R.id.address);
            phone = (TextView) item.findViewById(R.id.phone);

        }
    }
}
