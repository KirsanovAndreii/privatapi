package com.example.ankir.privatbankapi;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    public static final String FILE_NAME = "base_of_pb";
    List<ResponsePrivatApi> list = new ArrayList<>();
    List<ResponsePrivatApi> listFilter = new ArrayList<>();
    long timeAgo;
    RecyclerView rList;
    OfficeAdapter adapter;
    EditText editName;
    EditText editCity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editName = (EditText) findViewById(R.id.edit_name);
        editCity = (EditText) findViewById(R.id.edit_city);


        rList = (RecyclerView) findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rList.setLayoutManager(layoutManager);
        adapter = new OfficeAdapter(this, listFilter);
        rList.setAdapter(adapter);


        if (fileExistance(FILE_NAME)) {
            Toast.makeText(MainActivity.this, "read Disk", Toast.LENGTH_SHORT).show();
            readData();
        } else {
            Toast.makeText(MainActivity.this, "get for Internet", Toast.LENGTH_SHORT).show();
            getData();
        }

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listFilter.clear();
                listFilter.addAll(goFilter(list));
                adapter.notifyDataSetChanged();
            }
        });

        editCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listFilter.clear();
                listFilter.addAll(goFilter(list));
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void getData() {
        Retrofit.getApiPrivat(new Callback<List<ResponsePrivatApi>>() {
            @Override
            public void success(List<ResponsePrivatApi> responsePrivatApi, Response response) {
                Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();
                updataUI(responsePrivatApi);


                saveData(responsePrivatApi);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveData(List<ResponsePrivatApi> responsePrivatApi) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String json = new Gson().toJson(responsePrivatApi);
            fos.write((System.currentTimeMillis() + System.lineSeparator()).getBytes());

            fos.write(json.getBytes());
            fos.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            line = reader.readLine();
            timeAgo = Long.parseLong(line);

            if (System.currentTimeMillis() - timeAgo < 86400000)  //24 часа = 86 400 000млс
            {
                while ((line = reader.readLine()) != null) { // get text line by line, while text presen
                    json.append(line); // save text line to StringBuilder
                }
                fis.close(); //important: close reader
                Type listType = new TypeToken<ArrayList<ResponsePrivatApi>>() {
                }.getType();
                List<ResponsePrivatApi> responsePrivatApi = new Gson().fromJson(json.toString(), listType);
                updataUI(responsePrivatApi);
            } else {
                fis.close();
                Toast.makeText(MainActivity.this, "re get for Internet", Toast.LENGTH_SHORT).show();
                getData();
            }

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void updataUI(List<ResponsePrivatApi> responsePrivatApi) {
        list.clear();
        list.addAll(responsePrivatApi);
        listFilter.clear();
        listFilter.addAll(goFilter(list));
        adapter.notifyDataSetChanged();
        //      setTitle(getTitle() + " load a " + (System.currentTimeMillis() - timeAgo) / 1000 + " ago");
    }

    private boolean fileExistance(String fname) {
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }

    public List<ResponsePrivatApi> goFilter(List<ResponsePrivatApi> list) {
        String fName = editName.getText().toString();
        String fCity = editCity.getText().toString();
        List<ResponsePrivatApi> listOut = new ArrayList<>();

        if (fName.equals("") && fCity.equals("")) {
            return list;
        }// без изменений

        if (!fName.equals("") && !fCity.equals("")) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).name.contains(fName) && list.get(i).city.contains(fCity)) {
                    listOut.add(list.get(i));
                }
            }
        } else {
            if (!fName.equals("")) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).name.contains(fName)) {
                        listOut.add(list.get(i));
                    }
                }
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).city.contains(fCity)) {
                        listOut.add(list.get(i));
                    }
                }
            }
        }
        return listOut;
    }
}
